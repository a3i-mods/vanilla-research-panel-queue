﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace Vanilla_Research_Panel__Queue
{
    public class Alert_NeedPrerequisitesProject : Alert
    {
        public Alert_NeedPrerequisitesProject()
        {
            defaultLabel = "ResearchMissingImportantElement".Translate();
            defaultExplanation = "ResearchMissingImportantElement_Desc".Translate();
        }
        public override AlertReport GetReport()
        {
            RPEComponent component = Find.World.GetComponent<RPEComponent>();
            if (component._researchQueue.Count > 0)
            {
                var projectDef = DefDatabase<ResearchProjectDef>.GetNamedSilentFail(component._researchQueue[0]);
                if (projectDef.CanStartNow == false)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
