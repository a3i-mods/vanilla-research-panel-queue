﻿using RimWorld.Planet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

class RPEComponent : WorldComponent 
{
	public List<string> _researchQueue = new List<string>();
	
	public RPEComponent(World world) : base(world)
	{
	}
	public override void FinalizeInit()
	{
		base.FinalizeInit();
		
	}
	public override void WorldComponentTick()
	{
		base.WorldComponentTick();

		if(_researchQueue.Count > 0)
        {
			var projectDef = DefDatabase<ResearchProjectDef>.GetNamedSilentFail(_researchQueue[0]);

			if (projectDef.IsFinished)
			{
				_researchQueue.RemoveAt(0);
				if (_researchQueue.Count > 0)
				{

					projectDef = DefDatabase<ResearchProjectDef>.GetNamedSilentFail(_researchQueue[0]);
					if (projectDef != null && projectDef.CanStartNow)
					{
						Find.ResearchManager.currentProj = projectDef;
						foreach (var window in Find.WindowStack.Windows)
						{
							if (window.onlyOneOfTypeAllowed && window.GetType() == typeof(MainTabWindow_Research_New))
							{
								MainTabWindow_Research_New research = (window as MainTabWindow_Research_New);
								if (research != null)
								{
									research.SelectedProject = projectDef;
								}
							}
						}
					}
				}
			}
			else if(projectDef.CanStartNow)
            {
				Find.ResearchManager.currentProj = projectDef;
			}
			else if (!projectDef.CanStartNow)
			{
				Find.ResearchManager.currentProj = null;
			}
		}
	}
	public override void ExposeData()
	{
		Log.Message("Expose Data from new ");
		Scribe_Collections.Look(ref _researchQueue, "_researchQueue", LookMode.Value, new List<string>());
	}
}